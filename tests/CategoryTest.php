<?php

use App\Enums\HttpStatus;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;

class CategoryTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testPost()
    {
        $this->post('/category', [
            'name' => 'Categoria ' . str_random(5),
            'status' => true
        ], $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::CREATED, $this->response->status());
    }

    public function testPut()
    {
        $category = \App\Entities\Category::first();
        $this->put('/category/'.$category->id, [
            'name' => 'Categoria ' . str_random(5),
            'status' => true
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testGet()
    {
        $category = \App\Entities\Category::first();
        $this->get('/category/' . $category->id, $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testQuery()
    {
        $this->get('/category', $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testDelete()
    {
        $category = \App\Entities\Category::query()->orderBy('created_at', 'desc')->first();
        $this->delete('/category/' . $category->id, [], $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }
}