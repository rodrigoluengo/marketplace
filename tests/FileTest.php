<?php

use App\Entities\Item;
use App\Enums\HttpStatus;
use App\Entities\File;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Illuminate\Http\UploadedFile;

class FileTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testPost()
    {
        $this->post('/file', [
            'file' => UploadedFile::fake()->image('image_' . str_random(5) . '.jpg'),
            'id' => Item::first()->id,
            'target' => 'items'
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::CREATED, $this->response->status(), 'File created success');
    }

    public function testPut()
    {
        $file = File::first();
        $this->post('/file/' . $file->id, [
            'file' => UploadedFile::fake()->image('image_' . str_random(5) . '.jpg'),
            'target' => 'items',
            'public' => true
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status(), 'File updated success');
    }

    public function testGet()
    {
        $file = File::first();
        $this->get('/file/' . $file->id, $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->getStatusCode());
    }

    public function testDelete()
    {
        $file = File::first();
        $this->delete('/file/' . $file->id, [], $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }
}