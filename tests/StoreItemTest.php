<?php

use App\Entities\Item;
use App\Entities\StoreItem;
use App\Enums\HttpStatus;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;

class StoreItemTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testPost()
    {
        $item = Item::first();
        $this->post('/store-item', [
            'item_id' => $item->id,
            'status' => true,
            'stock_constraint' => false,
            'stock' => 10,
            'cost' => null,
            'price' => 19.99,
            'discount' => 0.07,
            'comission' => null,
            'metadata' => ['algumacoisa' => 'outracoisa']
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::CREATED, $this->response->status());
    }

    public function testPut()
    {
        $store_item = StoreItem::first();
        $this->put('/store-item/' . $store_item->id, [
            'status' => true,
            'stock_constraint' => true,
            'stock' => 10,
            'price' => 19.99,
            'metadata' => ['estoque_minimo' => 3]
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testGet()
    {
        $store_item = StoreItem::first();
        $this->get('/store-item/' . $store_item->id, $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testQuery()
    {
        $this->get('/store-item', $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }
}