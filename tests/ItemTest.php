<?php

use App\Enums\HttpStatus;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use App\Entities\Category;
use App\Entities\Item;
use \Illuminate\Support\Facades\DB;

class ItemTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testPost()
    {
        $this->post('/item', [
            'name' => 'Item ' . str_random(5),
            'status' => true,
            'category_id' => Category::query()->orderBy('created_at', 'desc')->first()->id
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::CREATED, $this->response->status(), 'Item created success');
    }

    public function testPut()
    {
        $item = Item::first();
        $this->put('/item/' . $item->id, [
            'name' => 'Item updated ' . str_random(5),
            'status' => false,
            'category_id' => Category::query()->orderBy('created_at', 'asc')->first()->id
        ], $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status(), 'Item updated success');
    }

    public function testGet()
    {
        $item = Item::first();
        $this->get('/item/' . $item->id, $this->getHeaderAuthorization());

        $this->assertEquals(HttpStatus::OK, $this->response->status(), 'Item get success');
    }

    public function testQuery()
    {
        $this->get('/item', $this->getHeaderAuthorization());
        $this->assertEquals(HttpStatus::OK, $this->response->status());
    }

    public function testDelete()
    {
        $item = DB::selectOne("select * from items where not exists(select 1 from store_items where item_id = items.id)");
        $this->assertNotNull($item);
        if ($item !== null) {
            $this->delete('/item/' . $item->id, [], $this->getHeaderAuthorization());
            $this->assertEquals(HttpStatus::OK, $this->response->status(), 'Item get success');
        }
    }
}