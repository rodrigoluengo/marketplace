<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('store_id')->nullable();
            $table->uuid('user_id')->nullable();
            $table->char('type', 1)->default('F');
            $table->boolean('status')->nullable();
            $table->string('name', 90);
            $table->string('company', 90)->nullable();
            $table->char('gender', 1)->nullable();
            $table->date('since')->nullable();
            $table->string('document', 18)->nullable();
            $table->jsonb('phones')->nullable();
            $table->string('details', 4000)->nullable();
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id')->nullable();
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');

            $table->unique('document');
        });

        DB::statement("ALTER TABLE people ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
