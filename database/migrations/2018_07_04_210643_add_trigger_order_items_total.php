<?php

use Illuminate\Database\Migrations\Migration;

class AddTriggerOrderItemsTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE FUNCTION fn_tg_order_items_total() RETURNS TRIGGER AS
            $$
            DECLARE
              _cost NUMERIC(18, 2);
              _price NUMERIC(18, 2);
              _discount_perc NUMERIC(18, 2);
              _commission_perc NUMERIC(18, 2);
            BEGIN
            
              IF (TG_OP = 'INSERT') THEN
                SELECT
                  items.cost,
                  items.price,
                  COALESCE(items.discount, categories.discount, stores.discount) AS discount,
                  COALESCE(items.commission, categories.commission, stores.commission) AS commission
                INTO
                  _cost,
                  _price,
                  _discount_perc,
                  _commission_perc
                FROM items
                  INNER JOIN categories ON items.category_id = categories.id
                  INNER JOIN stores ON items.owner_id = stores.owner_id
                WHERE items.id = NEW.item_id;
            
                NEW.cost = _cost;
                NEW.price = _price;
                NEW.discount_perc = _discount_perc;
                NEW.commission_perc = _commission_perc;
            
              END IF;
            
              IF (TG_OP = 'UPDATE') THEN
                _cost = OLD.cost;
                _price = OLD.price;
                _discount_perc = OLD.discount_perc;
                _commission_perc = OLD.commission_perc;
              END IF;
            
              IF _cost IS NOT NULL THEN
                NEW.cost_total = _cost * NEW.quantity;
              END IF;
              NEW.value = _price * NEW.quantity;
              
              NEW.unit_price = _price;
            
              IF _discount_perc IS NOT NULL THEN
                NEW.discount = NEW.value * _discount_perc;
                NEW.unit_price = _price - (_price * _discount_perc);
              END IF;
            
              IF NEW.discount IS NOT NULL THEN
                NEW.discount_total = NEW.value - NEW.discount;
              END IF;
            
              IF _commission_perc IS NOT NULL THEN
                NEW.commission = COALESCE(NEW.discount_total, NEW.value) * _commission_perc;
              END IF;
            
              NEW.commission_total = COALESCE(NEW.discount_total, NEW.value);
              NEW.total = COALESCE(NEW.discount_total, NEW.value) - COALESCE(NEW.commission, 0);
              NEW.lucre = CASE WHEN NEW.cost_total IS NOT NULL THEN NEW.total - NEW.cost_total ELSE NEW.total END;
            
              RETURN NEW;
            END;
            $$
            LANGUAGE plpgsql;");

        DB::statement("CREATE TRIGGER tg_order_items_total
                          BEFORE INSERT OR UPDATE ON order_items
                          FOR EACH ROW
                        EXECUTE PROCEDURE fn_tg_order_items_total();");*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*DB::statement("DROP TRIGGER IF EXISTS tg_order_items_total ON order_items CASCADE;");
        DB::statement("DROP FUNCTION IF EXISTS fn_tg_order_items_total();");*/
    }
}
