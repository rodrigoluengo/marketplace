<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('store_id');
            $table->integer('amount');
            $table->string('status', 25);
            $table->string('payment_method', 11);
            $table->json('request');
            $table->json('responses')->nullable();
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id')->nullable();
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');

            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE transactions ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
