<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('category_id')->nullable();
            $table->string('name', 125)->unique();
            $table->string('details', 4000)->nullable();
            $table->boolean('status')->nullable();
            $table->decimal('commission', 18,2)->nullable();
            $table->integer('position')->nullable();
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id');
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE categories ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
