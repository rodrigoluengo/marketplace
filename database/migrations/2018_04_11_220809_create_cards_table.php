<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('transaction_id');
            $table->uuid('address_id')->nullable();
            $table->string('external_id');
            $table->string('brand');
            $table->string('holder_name');
            $table->string('first_digits');
            $table->string('last_digits');
            $table->string('country');
            $table->string('fingerprint');
            $table->boolean('valid');
            $table->char('expiration_date', 4);
            $table->jsonb('metadata')->nullable();
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('address_id')->references('id')->on('addresses');
        });

        DB::statement("ALTER TABLE cards ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
