<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTriggerOrdersTotal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*DB::statement("
        CREATE OR REPLACE FUNCTION fn_tg_orders_total() RETURNS TRIGGER AS
        $$
        DECLARE
          _order_id UUID;
          _quantity NUMERIC(18, 4);
          _cost_total NUMERIC(18, 2);
          _value NUMERIC(18, 2);
          _discount_perc NUMERIC(18, 2);
          _discount NUMERIC(18, 2);
          _discount_total NUMERIC(18, 2);
          _commission_perc NUMERIC(18, 2);
          _commission NUMERIC(18, 2);
          _commission_total NUMERIC(18, 2);
          _total NUMERIC(18, 2);
          _lucre NUMERIC(18, 2);
        BEGIN
          IF (TG_OP = 'DELETE') THEN
            _order_id = OLD.order_id;
          ELSE
            _order_id = NEW.order_id;
          END IF;
          SELECT
            SUM(COALESCE(quantity, 0)),
            SUM(COALESCE(cost_total, 0)),
            SUM(COALESCE(value, 0)),
            SUM(COALESCE(discount, 0)),
            SUM(COALESCE(discount_total, value)),
            SUM(COALESCE(commission, 0)),
            SUM(COALESCE(commission_total, 0)),
            SUM(COALESCE(total, 0)),
            SUM(COALESCE(lucre, 0))
          INTO
            _quantity,
            _cost_total,
            _value,
            _discount,
            _discount_total,
            _commission,
            _commission_total,
            _total,
            _lucre
          FROM order_items
          WHERE order_id = _order_id;
          
          IF _cost_total = 0 THEN
            _cost_total = NULL;
          END IF;
          IF _discount = 0 THEN
            _discount = NULL;
            _discount_total = NULL;
          END IF;
        
          IF _discount > 0 THEN
            _discount_perc = (_value - _discount) / _value;
          END IF;
        
          IF _commission > 0 THEN
            _commission_perc = (COALESCE(_discount_total, _value) - _total) / COALESCE(_discount_total, _value);
          END IF;
          
          IF _lucre = 0 THEN
            _lucre = NULL;
          END IF;
        
          UPDATE orders SET
            quantity = _quantity,
            cost_total = _cost_total,
            value = _value,
            discount_perc = _discount_perc,
            discount = _discount,
            discount_total = _discount_total,
            commission_perc = _commission_perc,
            commission = _commission,
            commission_total = _commission_total,
            total = _total,
            lucre = _lucre
          WHERE id = _order_id;
        
          IF (TG_OP = 'DELETE') THEN
            RETURN OLD;
          ELSE
            RETURN NEW;
          END IF;
        END;
        $$
        LANGUAGE plpgsql;
        ");

        DB::statement("
        CREATE TRIGGER tg_orders_total
            AFTER INSERT OR UPDATE OR DELETE ON order_items
            FOR EACH ROW
            EXECUTE PROCEDURE fn_tg_orders_total();
        ");*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*DB::statement("DROP TRIGGER IF EXISTS tg_orders_total ON order_items CASCADE;");
        DB::statement("DROP FUNCTION IF EXISTS fn_tg_orders_total();");*/
    }
}
