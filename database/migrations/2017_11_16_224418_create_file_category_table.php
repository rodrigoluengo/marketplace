<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_category', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('file_id');
            $table->uuid('category_id');

            $table->foreign('file_id')->references('id')->on('files');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        DB::statement("ALTER TABLE file_category ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_category');
    }
}
