<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->boolean('status')->nullable();
            $table->string('email', 127)->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('password', 64);
            $table->char('role', 3)->default('CLI');
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id')->nullable();
            $table->uuid('updated_user_id')->nullable();

            $table->unique(['email', 'mobile']);

            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE users ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
