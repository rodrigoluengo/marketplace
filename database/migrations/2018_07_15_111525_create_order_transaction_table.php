<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transaction', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('order_id');
            $table->uuid('transaction_id');

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });

        DB::statement("ALTER TABLE order_transaction ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_order');
    }
}
