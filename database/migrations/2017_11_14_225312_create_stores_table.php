<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('user_id');
            $table->string('name', 127)->unique();
            $table->string('host', 127)->unique();
            $table->boolean('status')->nullable();
            $table->string('details', 4000)->nullable();
            $table->decimal('discount', 18,2)->nullable();
            $table->decimal('commission', 18,2);
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id');
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE stores ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
