<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('store_id');
            $table->uuid('item_id');
            $table->uuid('user_id')->nullable();
            $table->boolean('status')->nullable();
            $table->boolean('stock_constraint')->nullable();
            $table->decimal('stock', 18,3)->nullable();
            $table->decimal('cost', 18,2)->nullable();
            $table->decimal('price', 18,2);
            $table->decimal('discount', 18,2)->nullable();
            $table->decimal('commission', 18,2)->nullable();
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id');
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('store_id')->references('id')->on('stores');
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE store_items ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_items');
    }
}
