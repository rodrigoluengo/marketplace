<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('zipcode', 9);
            $table->string('street', 125);
            $table->string('street_number', 20);
            $table->string('complement', 125)->nullable();
            $table->string('neighborhood', 125);
            $table->string('city', 125);
            $table->string('state', 2);
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE addresses ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
