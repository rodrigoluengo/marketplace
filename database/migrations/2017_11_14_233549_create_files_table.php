<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('user_id')->nullable();
            $table->boolean('public');
            $table->string('filename');
            $table->string('name');
            $table->integer('size');
            $table->string('mime_type');
            $table->jsonb('metadata')->nullable();
            $table->integer('position')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id');
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE files ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
