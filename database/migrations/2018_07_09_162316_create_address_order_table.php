<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_order', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('address_id');
            $table->uuid('order_id');
            $table->string('name', 27);
            $table->boolean('default');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->jsonb('metadata')->nullable();
        });

        DB::statement("ALTER TABLE address_order ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_order');
    }
}
