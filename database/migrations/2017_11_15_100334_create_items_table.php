<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('category_id');
            $table->string('name', 200);
            $table->string('details', 4000)->nullable();
            $table->boolean('status')->nullable();
            $table->decimal('commission', 18,2)->nullable();
            $table->boolean('tangible')->default(true);
            $table->timestamps();
            $table->uuid('created_user_id');
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE items ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
