<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('store_id');
            $table->uuid('person_id')->nullable();
            $table->uuid('address_id')->nullable();
            $table->boolean('status')->nullable();
            $table->decimal('quantity', 18,4)->default(0);
            $table->decimal('cost_total', 18,2)->nullable();
            $table->decimal('value', 18,2)->default(0);
            $table->decimal('discount_perc', 18,2)->nullable();
            $table->decimal('discount', 18,2)->nullable();
            $table->decimal('discount_total', 18,2)->nullable();
            $table->string('details', 4000)->nullable();
            $table->decimal('commission_perc', 18,2)->default(0);
            $table->decimal('commission', 18,2)->default(0);
            $table->decimal('commission_total', 18,2)->default(0);
            $table->decimal('total', 18,2)->default(0);
            $table->decimal('lucre', 18,2)->nullable();
            $table->jsonb('metadata')->nullable();
            $table->timestamps();
            $table->uuid('created_user_id')->nullable();
            $table->uuid('updated_user_id')->nullable();

            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('created_user_id')->references('id')->on('users');
            $table->foreign('updated_user_id')->references('id')->on('users');
        });

        DB::statement("ALTER TABLE orders ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
