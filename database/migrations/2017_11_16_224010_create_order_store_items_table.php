<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_store_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('order_id');
            $table->uuid('store_item_id');
            $table->string('details', 1000)->nullable();
            $table->decimal('quantity', 18,4);
            $table->decimal('cost', 18,2)->nullable();
            $table->decimal('cost_total', 18,2)->nullable();
            $table->decimal('price', 18,2);
            $table->decimal('value', 18,2);
            $table->decimal('discount_perc', 18,2)->nullable();
            $table->decimal('discount', 18,2)->nullable();
            $table->decimal('discount_total', 18,2)->nullable();
            $table->decimal('unit_price', 18,2);
            $table->decimal('commission_perc', 18,2);
            $table->decimal('commission', 18,2);
            $table->decimal('commission_total', 18,2);
            $table->decimal('total', 18,2);
            $table->decimal('lucre', 18,2)->nullable();
            $table->jsonb('metadata')->nullable();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('store_item_id')->references('id')->on('store_items');
            $table->unique(['order_id', 'store_item_id']);
        });

        DB::statement("ALTER TABLE order_store_items ALTER COLUMN id SET DEFAULT uuid_generate_v4();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
