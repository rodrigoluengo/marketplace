<?php

use App\Entities\User;
use App\Entities\Person;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $user = new User([
                'email' => 'email@pontualracoes.com.br',
                'password' => 'secreto'
            ]);
            $user->setAttribute('status', true);
            $user->setAttribute('role', 'ADM');
            $user->setAttribute('created_at', Carbon::now());
            $user->save();
            $person = new Person([
                'name' => 'Administrador',
                'status' => true
            ]);
            $person->createdUser()->associate($user);
            $user->person()->save($person);

            $user = new User([
                'email' => 'rpl@outlook.com',
                'password' => 'krirho52'
            ]);
            $user->setAttribute('status', true);
            $user->setAttribute('role', 'CLI');
            $user->setAttribute('created_at', Carbon::now());
            $user->save();
            $person = new Person([
                'name' => 'Rodrigo Pereira Luengo',
                'status' => true,
                'type' => 'F',
                'gender' => 'M',
                'since' => '1983-10-29',
                'document' =>'330.088.868-92',
                'phones' => [
                    [
                        'name' => 'Principal',
                        'default' => true,
                        'number' => '(11) 95476-3954',
                        'whatsapp' => true
                    ]
                ]
            ]);
            $person->createdUser()->associate($user);
            $user->person()->save($person);
        });
    }
}
