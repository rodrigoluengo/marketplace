<?php

use App\Entities\Item;
use App\Entities\Store;
use App\Entities\StoreItem;
use App\Entities\User;
use Illuminate\Database\Seeder;

class StoreItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $store = Store::where('name', 'Pontual Rações')->first();
        $user = User::where('email', 'email@pontualracoes.com.br')->first();
        $items = Item::get();

        $store_items = [
            [
                'status' => true,
                'stock' => 10,
                'price' => 10,
            ],
            [
                'status' => true,
                'stock' => 1,
                'stock_constraint' => true,
                'price' => 20,
            ],
            [
                'status' => true,
                'cost' => 20,
                'price' => 30,
            ],
            [
                'status' => true,
                'discount' => 0.1,
                'price' => 40,
            ],
            [
                'status' => true,
                'commission' => 0.1,
                'price' => 40,
            ]
        ];
        foreach($items as $i => $item) {
            $store_item = new StoreItem($store_items[$i % 4]);
            $store_item->setAttribute('store_id', $store->id);
            $store_item->setAttribute('item_id', $item->id);
            $store_item->createdUser()->associate($user);
            $store_item->save();
        }
    }
}
