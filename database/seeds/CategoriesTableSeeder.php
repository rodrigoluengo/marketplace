<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'email@pontualracoes.com.br')->first();

        DB::table('categories')->insert([
            'name' => 'Categoria',
            'status' => true,
            'position' => 0,
            'created_user_id' =>$user->id,
            'created_at' => Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Categoria com 5% de desconto',
            'status' => true,
            'position' => 1,
            'created_user_id' =>$user->id,
            'created_at' => Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Categoria com 5% de comissão',
            'status' => true,
            'position' => 2,
            'created_user_id' =>$user->id,
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
