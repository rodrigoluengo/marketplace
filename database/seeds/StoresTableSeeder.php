<?php

use App\Entities\Store;
use App\Entities\User;
use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {
            $user = User::query()->where('email', 'email@pontualracoes.com.br')->first();
            $store = new Store([
                'name' => 'Pontual Rações',
                'host' => 'pontualracoes.localhost',
                'status' => true,
                'commission' => 0.07
            ]);
            $store->user()->associate($user);
            $store->createdUser()->associate($user);
            $store->save();
        });
    }
}
