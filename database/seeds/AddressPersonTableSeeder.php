<?php

use App\Entities\Address;
use App\Entities\User;
use Illuminate\Database\Seeder;

class AddressPersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'rpl@outlook.com')->first();
        $address = new Address([
            'zipcode' => '08940-000',
            'street' => 'Virgilina de Conceição Camargo',
            'street_number' => '58',
            'neighborhood' => 'Jardim Alvorada',
            'city' => 'Biritiba-Mirim',
            'state' => 'SP'
        ]);
        $user->person->addresses()->save($address, ['name' => 'Principal', 'default' => true]);
    }
}
