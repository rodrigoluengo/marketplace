<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
        $this->call('AddressPersonTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('ItemsTableSeeder');
        $this->call('StoresTableSeeder');
        $this->call('StoreItemsTableSeeder');
    }
}
