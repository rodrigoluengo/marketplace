<?php

use App\Entities\Category;
use App\Entities\Item;
use App\Entities\User;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'email@pontualracoes.com.br')->first();
        $categories = Category::get();

        $items = [
            [
                'name' => 'Produto com estoque',
                'status' => true,
                'tangible' => true
            ],
            [
                'name' => 'Produto com estoque e com verificação de estoque',
                'status' => true,
                'tangible' => true
            ],
            [
                'name' => 'Produto com preço de custo',
                'status' => true,
                'tangible' => true
            ],
            [
                'name' => 'Produto com desconto de 10%',
                'status' => true,
                'tangible' => true
            ],
            [
                'name' => 'Produto com comissão de 10%',
                'status' => true,
                'tangible' => true
            ]
        ];
        foreach($categories as $category) {
            foreach ($items as $value) {
                $value['name'] = $category->name . ' - ' .$value['name'];
                $value['category_id'] = $category->id;
                $item = new Item($value);
                $item->createdUser()->associate($user);
                $item->save();
            }
        }
    }
}
