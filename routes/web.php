<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Auth
$router->post('/auth', 'AuthController@post');

// Password
$router->post('/password-reset', 'PasswordResetController@post');
$router->get('/password-reset/{token}', 'PasswordResetController@get');
$router->put('/password-reset', 'PasswordResetController@put');

// Transaction
$router->post('/transaction/post-back/{id}', 'TransactionController@postBack');

$router->group(['middleware' => ['store']], function () use ($router) {

    $router->get('/category', 'CategoryController@query');
    $router->get('/category/{id}', 'CategoryController@get');

    $router->get('/item', 'ItemController@query');
    $router->get('/item/{id}', 'ItemController@get');

    $router->post('/user', 'UserController@post');

    $router->post('/order-item', 'OrderItemController@post');
    $router->put('/order-item/{id}', 'OrderItemController@put');
    $router->delete('/order-item/{id}', 'OrderItemController@delete');


    $router->post('/order', 'OrderController@post');
    $router->get('/order/{id}', 'OrderController@get');
    $router->put('/order/{id}', 'OrderController@put');
    $router->delete('/order/{id}', 'OrderController@delete');
    $router->post('/order/{id}/payment', 'OrderController@payment');

    $router->get('/store/{id}', 'StoreController@get');
});

$router->group(['middleware' => ['auth:api', 'refresh']], function () use ($router) {

    // Auth
    $router->delete('/auth', 'AuthController@delete');
    $router->get('/auth', 'AuthController@get');

    // Category
    $router->post('/category', 'CategoryController@post');
    $router->get('/categories/jstree', 'CategoryController@jstree');
    $router->put('/categories/move', 'CategoryController@move');
    $router->put('/category/{id}', 'CategoryController@put');
    $router->delete('/category/{id}', 'CategoryController@delete');

    // Item
    $router->post('/item', 'ItemController@post');
    $router->put('/item/{id}', 'ItemController@put');
    $router->delete('/item/{id}', 'ItemController@delete');

    //Store Item
    $router->post('/store-item', 'StoreItemController@post');
    $router->put('/store-item/{id}', 'StoreItemController@put');
    $router->get('/store-item/{id}', 'StoreItemController@get');
    $router->get('/store-item', 'StoreItemController@query');

    // File
    $router->post('/file', 'FileController@post');
    $router->post('/file/{id}', 'FileController@put');
    $router->get('/file/{id}', 'FileController@get');
    $router->delete('/file/{id}', 'FileController@delete');

    // Person
    $router->post('/person', 'PersonController@post');
    $router->put('/person/{id}', 'PersonController@put');
    $router->get('/person/{id}', 'PersonController@get');
    $router->get('/person', 'PersonController@search');

    // Federal Receive Document
    $router->get('/federal-receive-document', 'FederalReceiveDocumentController@get');
    $router->post('/federal-receive-document', 'FederalReceiveDocumentController@post');

    // Address
    $router->get('/address/zip-code', 'AddressController@zipCode');

    // Store
    $router->post('/store', 'StoreController@post');

    // Transaction
    $router->get('/transaction/card-hash-key', 'TransactionController@cardHashKey');

    // Order

});
