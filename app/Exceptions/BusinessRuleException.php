<?php
namespace App\Exceptions;

use Exception;
use Throwable;

class BusinessRuleException extends Exception
{
    public $category;

    public function __construct($category, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->category = $category;
    }
}
