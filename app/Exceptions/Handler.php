<?php
namespace App\Exceptions;

use App\Enums\HttpStatus;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof NotFoundHttpException) {
            return response()->json(['error' => 'resource_not_found'], HttpStatus::NOT_FOUND);
        }

        if ($e instanceof ModelNotFoundException) {
            return response()->json(['error' => $e->getMessage()], HttpStatus::NOT_FOUND);
        }

        if ($e instanceof AuthorizationException) {
            return response()->json(['error' => $e->getMessage()], HttpStatus::UNAUTHORIZED);
        }

        if ($e instanceof UnauthorizedHttpException) {
            return response()->json(['error' => 'Unauthorized'], HttpStatus::UNAUTHORIZED);
        }

        if ($e instanceof MethodNotAllowedHttpException) {
            return response()->json(['error' => 'method_not_allowed'], HttpStatus::METHOD_NOT_ALLOWED);
        }

        if ($e instanceof RelationshipException) {
            return response()->json($e->errors, HttpStatus::BAD_REQUEST);
        }

        if ($e instanceof ValidatorException) {
            return response()->json($e->errors, HttpStatus::UNPROCESSABLE_ENTITY);
        }

        if ($e instanceof BusinessRuleException) {
            return response()->json([$e->category => $e->getMessage()], HttpStatus::BAD_REQUEST);
        }

        return parent::render($request, $e);
    }
}
