<?php
namespace App\Exceptions;

use Throwable;

class RelationshipException extends ValidatorException
{
    public function __construct($error, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct(['error' => [$error]], $error, $code, $previous);
    }
}
