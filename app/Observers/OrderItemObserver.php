<?php
namespace App\Observers;

use App\Entities\OrderStoreItem;
use App\Repositories\OrderRepository;

class OrderItemObserver
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function created(OrderStoreItem $orderItem)
    {
        $this->orderRepository->calculate($orderItem->order);
    }

    public function updated(OrderStoreItem $orderItem)
    {
        $this->orderRepository->calculate($orderItem->order);
    }

    public function deleted(OrderStoreItem $orderItem)
    {
        $this->orderRepository->calculate($orderItem->order);
    }
}
