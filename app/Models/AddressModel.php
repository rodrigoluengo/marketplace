<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Auth;
use Illuminate\Validation\Rule;
use Validator;

class AddressModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'zipcode' => 'required|max:9',
            'street' => 'required|max:125',
            'street_number' => 'required|max:20',
            'complement' => 'max:45',
            'neighborhood' => 'required|max:45',
            'city' => 'required|max:32',
            'state' => 'required|in:AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,RJ,RN,RS,RO,RR,SC,SP,SE,TO'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
