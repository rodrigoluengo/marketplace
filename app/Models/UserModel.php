<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Validator;

class UserModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'email' => 'required|max:127|unique:users',
            'password' => 'required|min:6|max:20|confirmed',
            'status' => 'nullable|boolean'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
