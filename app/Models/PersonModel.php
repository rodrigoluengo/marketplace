<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Entities\Owner;
use App\Traits\Model\ValidatorModelTrait;
use Auth;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Validator;

class PersonModel implements ModelInterface
{
    use ValidatorModelTrait;

    private $addressModel;
    private $userModel;

    public function __construct(AddressModel $addressModel, UserModel $userModel)
    {
        $this->addressModel = $addressModel;
        $this->userModel = $userModel;
    }

    public function validate(array &$data, $id = null): bool
    {
        $owner = app(Owner::class);
        $rules = [
            'type' => 'required|in:F,J',
            'name' => 'required|max:90',
            'status' => 'required|boolean',
            'company' => 'max:90',
            'gender' => 'required_if:type,F|in:M,F',
            'since' => 'date|before_or_equal:' . Carbon::now()->format('Y-m-d'),
            'document' => [
                'required',
                Rule::unique('people')
                    ->where('owner_id', $owner->id)
                    ->ignore($id),
                'cpf_cnpj'
            ],
            'addresses' => 'required|array|max:5',
            'addresses.*.name' => 'required|distinct|max:32',
            'addresses.*.default' => 'boolean',
            'phones' => 'required|array|max:5',
            'phones.*.name' => 'required|distinct|max:32',
            'phones.*.default' => 'boolean',
            'phones.*.number' => 'required|distinct',
            'phones.*.whatsapp' => 'boolean',
            'details' => 'max:4000',
            'user' => 'array',
        ];

        $this->validator = Validator::make($data, $rules);

        $addresses = array_get($data, 'addresses');
        if (is_array($addresses)) {
            foreach ($addresses as $i => $address) {
                $address_id = array_get($address, 'id');
                if (!$this->addressModel->validate($address, $address_id)) {
                    $this->addErrors("addresses.$i", $this->addressModel->getErrors());
                }
            }
        }

        $phones = array_get($data, 'phones');

        $this->validator->after(function ($validator) use ($addresses, $phones) {

            $default_addresses = collect($addresses)->filter(function ($address) {
                return $address['default'] === true;
            });

            if ($default_addresses->count() > 1) {
                $index = array_search($default_addresses->first(), $addresses);
                $validator->errors()->add("addresses.$index.default", 'address.default.uniqueness');
            }

            if ($default_addresses->isEmpty()) {
                $validator->errors()->add("addresses.0.default", 'address.default.required');
            }

            $default_phones = collect($phones)->filter(function ($phone) {
                return $phone['default'] === true;
            });

            if ($default_phones->count() > 1) {
                $index = array_search($default_phones->first(), $phones);
                $validator->errors()->add("phones.$index.default", 'phone.default.uniqueness');
            }

            if ($default_phones->isEmpty()) {
                $validator->errors()->add("phones.0.default", 'phone.default.required');
            }
        });

        $user = array_get($data, 'user');
        if ($user !== null && !$this->userModel->validate($user)) {
            $this->addErrors('user', $this->userModel->getErrors());
        }

        return $this->isValid();
    }
}
