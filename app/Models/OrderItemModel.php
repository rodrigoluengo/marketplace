<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Entities\Store;
use App\Repositories\ItemRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Traits\Model\ValidatorModelTrait;
use Illuminate\Validation\Rule;
use Validator;

class OrderItemModel implements ModelInterface
{
    use ValidatorModelTrait;

    private $itemRepository;
    private $orderItemRepository;
    private $orderRepository;

    public function __construct(
        ItemRepository $itemRepository,
        OrderItemRepository $orderItemRepository,
        OrderRepository $orderRepository
    )
    {
        $this->itemRepository = $itemRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->orderRepository = $orderRepository;
    }

    public function validate(array &$data, $id = null): bool
    {
        $this->resetErrors();
        $store = app(Store::class);
        $rules = [
            'item_id' => [
                'required',
                Rule::exists('items', 'id')
                    ->where('owner_id', $store->owner->id)
                    ->where('status', true)
            ],
            'quantity' => 'required|numeric|min:0'
        ];
        $order_item = $this->orderItemRepository->find($id);
        $order_id = array_get($data, 'order_id') ?? $order_item->order_id ?? null;
        if ($order_id !== null && $this->orderRepository->existsStore($order_id, $store->id) === false) {
            unset($data['order_id']);
        }

        $this->validator = Validator::make($data, $rules);

        $item_id = array_get($data, 'item_id');
        $quantity = array_get($data, 'quantity');

        if ($item_id !== null && is_numeric($quantity)) {
            $item = $this->itemRepository->find($item_id);
            if ($item !== null && $item->stock_constraint === true) {
                $stock  = $this->itemRepository->stock($data);
                $stock -= $this->orderItemRepository->quantity($item_id, $order_id);
                $stock -= $quantity;
                if ($stock < 0) {
                    $this->addError('quantity', 'item.quantity.stock.less');
                }
            }
        }

        return $this->isValid();
    }
}