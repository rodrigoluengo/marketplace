<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Entities\Owner;
use App\Repositories\BuyRepository;
use App\Repositories\ItemRepository;
use App\Traits\Model\ValidatorModelTrait;
use Illuminate\Validation\Rule;
use Validator;

class BuyModel implements ModelInterface
{
    use ValidatorModelTrait;

    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'item_id' => ['required', Rule::exists('items', 'id')->where(function ($query) {
                $owner = app(Owner::class);
                $query->where('owner_id', $owner->id);
            })],
            'quantity' => 'required|numeric|min:0.001',
            'session_id' => 'required'
        ];

        $this->validator = Validator::make($data, $rules);

        $item_id = array_get($data, 'item_id');
        $quantity = array_get($data, 'quantity');

        if ($item_id !== null && is_numeric($quantity)) {
            $item = $this->itemRepository->find($item_id);
            if ($item->stock_constraint === true && $this->itemRepository->stock($data) < 0) {
                $this->addError('quantity', 'item.quantity.stock.less');
            }
        }

        return $this->isValid();
    }
}
