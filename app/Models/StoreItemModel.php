<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Illuminate\Support\Facades\Validator;

class StoreItemModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'status' => 'nullable|boolean',
            'stock_constraint' => 'nullable|boolean',
            'stock' => 'required_if:stock_constraint,true|numeric',
            'cost' => 'nullable|numeric',
            'price' => 'required|numeric',
            'discount' => 'nullable|numeric|min:0|max:1',
            'comission' => 'nullable|numeric|min:0|max:1',
            'metadadata' => 'nullable|array'
        ];

        if ($id === null) {
            $rules['item_id'] = 'required|exists:items,id';
        }

        $this->validator = Validator::make($data, $rules);
        return $this->isValid();
    }
}
