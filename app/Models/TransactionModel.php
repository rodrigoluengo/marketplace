<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Entities\Store;
use App\Traits\Model\ValidatorModelTrait;
use Illuminate\Validation\Rule;
use Validator;

class TransactionModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $store = app(Store::class);

        $rules = [
            'amount' => 'required|integer',
            'payment_method' => 'required|in:credit_card,boleto'
        ];

        $payment_method = array_get($data, 'payment_method');
        if ($payment_method === 'credit_card') {
            $card_hash = array_get($data, 'card_hash');
            $card_id = array_get($data, 'card_id');

            if ($card_hash === null && $card_id === null) {
                $rules['card_holder_name'] = 'required';
                $rules['card_expiration_date'] = 'required';
                $rules['card_number'] = 'required';
                $rules['card_cvv'] = 'required';
            }

            $rules['installments'] = 'required|integer|min:1|max:12';
            $rules['customer'] = 'required|array';
            $rules['customer.external_id'] = 'required';
            $rules['customer.name'] = 'required';
            $rules['customer.type'] = 'required|in:individual,corporation';
            $rules['customer.country'] = 'required';
            $rules['customer.email'] = 'required|email';
            $rules['billing'] = 'required|array';
            $rules['items'] = 'required|array';
        } elseif ($payment_method === 'boleto') {
            $rules['installments'] = 'integer|min:1|max:1';
            $rules['boleto_expiration_date'] = 'date|after:now';
        }

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }

}
