<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Validator;

class StoreModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'name' => 'required|max:127',
            'status' => 'boolean',
            'details' => 'max:4000',
            'discount' => 'numeric|min:0.00',
            'commission' => 'numeric|min:0.00'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
