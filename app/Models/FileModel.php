<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Auth;
use DB;
use Validator;

class FileModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'file' => ['file', 'required'],
            'target' => 'in:categories,items',
            'id' => 'required_if:target,!==,null',
            'public' => ['boolean'],
            'position' => ['numeric', 'min:0']
        ];

        $this->validator = Validator::make($data, $rules);

        $target = array_get($data, 'target');
        $id = array_get($data, 'id');

        $this->validator->after(function($validator) use ($target, $id) {
            $exists = DB::table($target)->where('id', $id)->exists();
            if ($target !== null && $id !== null && $exists === false) {
                $validator->errors()->add('target', 'target.id.invalid');
            }
        });

        return $this->isValid();
    }
}
