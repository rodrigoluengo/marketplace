<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Repositories\OrderRepository;
use App\Repositories\PersonRepository;
use App\Traits\Model\ValidatorModelTrait;
use DB;
use Validator;

class OrderModel implements ModelInterface
{
    use ValidatorModelTrait;

    private $repository;
    private $personRepository;
    private $orderItemModel;

    public function __construct(OrderRepository $repository, PersonRepository $personRepository, OrderItemModel $orderItemModel)
    {
        $this->repository = $repository;
        $this->personRepository = $personRepository;
        $this->orderItemModel = $orderItemModel;
    }

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'person_id'     => ['nullable', 'exists:people,id'],
            'address_id'    => ['nullable', 'exists:addresses,id'],
            'status'        => 'nullable|boolean',
            'order_items'   => 'required|array',
        ];

        $person_id  = array_get($data, 'person_id');
        $status     = array_get($data, 'status');
        $address_id = array_get($data, 'address_id');

        if ($status === true) {
            $rules['person_id'][] = 'required';
            $rules['address_id'][] = 'required';
        }

        if ($person_id !== null && $address_id !== null && $this->personRepository->addressExists($person_id, $address_id) === false) {
            $this->addError('address_id', 'address_person_invalid');
        }

        $this->validator = Validator::make($data, $rules);

        $order_items = array_get($data, 'order_items');
        if ($order_items !== null) {
            foreach ($order_items as $i => $order_item) {
                if ($this->orderItemModel->validate($order_item) === false) {
                    $this->addErrors("order_items.$i", $this->orderItemModel->getErrors());
                }
            }
        }

        return $this->isValid();
    }

    public function remove($id)
    {
        $rules = [
            'id' => 'required|exists:orders'
        ];
        $this->validator = Validator::make(['id' => $id], $rules);
        return $this->isValid();
    }

    public function validatePayment(array &$data, $id)
    {
        $order = $this->repository->find($id);
        if ($order->isPayed() === true) {
            $this->addError('order', 'Pedido já está pago');
        }

        // TODO: Verify if transaction is boleto and date is before end date
    }
}
