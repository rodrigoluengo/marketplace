<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Validator;

class PasswordResetModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'email' => 'required|email|exists:users'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }

    public function updateValidate(array &$data)
    {
        $rules = [
            'email' => 'required|email',
            'token' => 'required',
            'password' => 'required|min:6|confirmed'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
