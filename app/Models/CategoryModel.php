<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Auth;
use Illuminate\Validation\Rule;
use Validator;

class CategoryModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'category_id' => 'nullable|exists:categories,id',
            'name' => [
                'required',
                'max:125',
                Rule::unique('categories')->ignore($id)
            ],
            'details' => 'nullable|max:4000',
            'status' => 'required|boolean',
            'discount' =>'nullable|numeric|min:0.00|max:100',
            'commission' =>'nullable|numeric|min:0.00|max:100'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
