<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Entities\Store;
use App\Traits\Model\ValidatorModelTrait;
use Auth;
use Illuminate\Validation\Rule;
use Validator;

class ItemModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'category_id' => 'required|exists:categories,id',
            'name' => [
                'required',
                'max:200',
                Rule::unique('items')
                    ->ignore($id)
            ],
            'details' => 'max:4000',
            'status' => 'required|boolean',
            /*'stock_constraint' =>'nullable|boolean',
            'stock' =>'nullable|numeric',
            'cost' =>'nullable|numeric|min:0.00',
            'price' =>'required|numeric|min:0.00',
            'discount' =>'nullable|numeric|min:0.00|max:100',*/
            'commission' =>'nullable|numeric|min:0.00|max:100'
        ];

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }

}
