<?php
namespace App\Models;

use App\Contracts\Model\ModelInterface;
use App\Traits\Model\ValidatorModelTrait;
use Validator;

class FederalReceiveDocumentModel implements ModelInterface
{
    use ValidatorModelTrait;

    public function validateCaptcha(array &$data, $id = null): bool
    {
        $rules = [
            'type'      => 'required|in:F,J',
            'document'  => ['required'],
            'since'     => 'required_if:type,F|before:today'
        ];

        $type = array_get($data, 'type');
        if ($type === 'F') {
            $rules['document'][] = 'cpf';
        }
        if ($type === 'J') {
            $rules['document'][] = 'cnpj';
        }

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }

    public function validate(array &$data, $id = null): bool
    {
        $rules = [
            'type'          => 'required|in:F,J',
            'document'      => ['required'],
            'since'         => 'required_if:type,F|before:today',
            'cookie'        => 'required',
            'captcha_text'  => 'required'
        ];

        $type = array_get($data, 'type');
        if ($type === 'F') {
            $rules['document'][] = 'cpf';
        }
        if ($type === 'J') {
            $rules['document'][] = 'cnpj';
        }

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}