<?php
namespace App\Services;

use App\Exceptions\RelationshipException;
use App\Exceptions\ValidatorException;
use App\Models\ItemModel;
use App\Repositories\ItemRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ItemService
{
    private $model;
    private $repository;

    public function __construct(ItemModel $model, ItemRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $item = null;
        if ($id !== null and ($item = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('item.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($item, $data);
    }

    public function find($id)
    {
        $item = $this->repository->find($id);

        if ($item === null) {
            throw new ModelNotFoundException('item.not_found');
        }

        return $item;
    }

    public function query(array $data)
    {
        return $this->repository->query($data);
    }

    public function delete($id)
    {
        $item = $this->repository->find($id);

        if ($item === null) {
            throw new ModelNotFoundException('item.not_found');
        }

        // TODO: verify if exists item relationships
        if ($this->repository->hasStoreItems($id)) {
            throw new RelationshipException('store_items.relationship');
        }

        // TODO: remove all files item

        return $this->repository->delete($item);
    }
}
