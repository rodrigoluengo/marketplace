<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\OrderItemModel;
use App\Repositories\OrderItemRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderItemService
{
    private $model;
    private $repository;

    public function __construct(OrderItemModel $model, OrderItemRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $order_item = null;
        $item_id = array_get($data, 'item_id');
        $order_id = array_get($data, 'order_id');

        if ($id !== null and ($order_item = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('order_item.not_found');
        }

        if ($item_id !== null && $order_id !== null && $id === null) {
            $order_item = $this->repository->findByItemIdAndOrderId($item_id, $order_id);
            if ($order_item !== null && isset($data['quantity']) && is_numeric($data['quantity'])) {
                $id = $order_item->id;
                $quantity = $order_item->quantity;
                $data['quantity'] += $quantity;
            }
        }

        if (!$this->model->validate($data, $id)) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id !== null) {
            return $this->repository->update($order_item, $data);
        }

        return $this->repository->insert($data);
    }

    public function delete($id)
    {
        $order_item = $this->repository->find($id);

        if ($order_item === null) {
            throw new ModelNotFoundException('order_item.not_found');
        }

        // TODO: verify if order_item can delete

        return $this->repository->delete($order_item);
    }
}
