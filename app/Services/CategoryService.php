<?php
namespace App\Services;

use App\Exceptions\RelationshipException;
use App\Exceptions\ValidatorException;
use App\Models\CategoryModel;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryService
{
    private $model;
    private $repository;

    public function __construct(CategoryModel $model, CategoryRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $category = null;
        if ($id !== null and ($category = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('category.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($category, $data);
    }

    public function find($id)
    {
        $category = $this->repository->find($id);

        if ($category === null) {
            throw new ModelNotFoundException('category.not_found');
        }

        return $category;
    }

    public function query(array $data)
    {
        return $this->repository->query($data)->map(function ($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'categories' => $this->query(['category_id' => $category->id])
            ];
        });
    }

    public function delete($id)
    {
        $category = $this->repository->find($id);

        if ($category === null) {
            throw new ModelNotFoundException('category.not_found');
        }

        if ($this->repository->categoryExists($id)) {
            throw new RelationshipException('category.category.relationship.exists');
        }

        if ($this->repository->itemExists($id)) {
            throw new RelationshipException('category.item.relationship.exists');
        }

        // TODO: remove all files category

        return $this->repository->delete($category);
    }
}