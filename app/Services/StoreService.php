<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\StoreModel;
use App\Repositories\StoreRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StoreService
{
    private $model;
    private $repository;

    public function __construct(StoreModel $model, StoreRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $store = null;
        if ($id !== null and ($store = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('store.not_found');
        }

        if (!$this->model->validate($data, $id)) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id !== null) {
            return $this->repository->update($store, $data);
        }

        return $this->repository->insert($data);
    }

    public function find($id)
    {
        return $this->repository->findIdOrDomain($id);
    }
}
