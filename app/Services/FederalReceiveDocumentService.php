<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\FederalReceiveDocumentModel;
use App\Repositories\FederalReceiveDocumentRepository;

class FederalReceiveDocumentService
{
    private $model;
    private $repository;

    public function __construct(FederalReceiveDocumentModel $model, FederalReceiveDocumentRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function captcha($data)
    {
        if (!$this->model->validateCaptcha($data)) {
            throw new ValidatorException($this->model->getErrors());
        }
        return $data['type'] === 'F' ? $this->repository->captchaCpf() : $this->repository->captchaCnpj();
    }

    public function search($data)
    {
        if (!$this->model->validate($data)) {
            throw new ValidatorException($this->model->getErrors());
        }
        return $data['type'] === 'F' ? $this->repository->searchCpf($data) : $this->repository->captchaCnpj();
    }
}