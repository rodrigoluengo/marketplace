<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\UserModel;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService
{
    private $model;
    private $repository;

    public function __construct(UserModel $model, UserRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $store = null;
        if ($id !== null and ($store = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('user.not_found');
        }

        if (!$this->model->validate($data, $id)) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id !== null) {
            return $this->repository->update($store, $data);
        }

        return $this->repository->insert($data);
    }
}
