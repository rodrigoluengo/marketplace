<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\OrderModel;
use App\Repositories\OrderRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderService
{
    private $model;
    private $repository;
    private $transactionService;

    public function __construct(OrderModel $model, OrderRepository $repository, TransactionService $transactionService)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->transactionService = $transactionService;
    }

    public function save(array $data, $id = null)
    {
        $order = null;
        if ($id !== null && ($order = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('order.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($order, $data);
    }

    public function find($id)
    {
        $order = $this->repository->find($id);
        if ($order === null) {
            throw new ModelNotFoundException('order.not_found');
        }
        return $order;
    }

    public function remove($id)
    {
        if (!$this->model->remove($id)) {
            throw new ValidatorException($this->model->getErrors());
        }
        $order = $this->repository->find($id);
        $this->repository->delete($order);
    }

    public function payment(array $data, $id)
    {
        $order = $this->repository->find($id);
        if ($order === null) {
            throw new ModelNotFoundException('order.not_found');
        }

        if ($this->model->validatePayment($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        $data['postback_url'] = env('PAGAR_ME_POSTBACK_URL') . 'post-back/' . $id;
        $data['amount'] = number_format($order->commission_total, 2, '', '');
        $costumer = [
            'external_id' => (string) $order->person_id,
            'name' => $order->person->name,
            'type' => $order->person->type === 'F' ? 'individual' : 'corporation',
            'country' => 'br',
            'email' => $order->person->user->email,
            'documents' => [[
                'type' => $order->person->type === 'F' ? 'cpf' : 'cnpj',
                'number' => $order->person->document
            ]],
            "phone_numbers" => collect($order->person->phones)->map(function ($value) {
                return '+55' . preg_replace("/[^0-9]/", '', $value['number']);
            })
        ];
        if ($order->person->type === 'F' && $order->person->since !== null) {
            $costumer['birthday'] = $order->person->since->toDateString();
        }

        $data['customer'] = $costumer;
        $data['billing']['name'] = $data['card_holder_name'];
        $data['billing']['address']['country'] = 'br';
        $data['billing']['address']['zipcode'] = preg_replace("/[^0-9]/", '', $data['billing']['address']['zipcode']);
        unset($data['billing']['address']['complement']);
        $data['items'] = $order->orderItems->map(function ($orderItem) {
            return [
                'id' => (string) $orderItem->item->id,
                'title' => $orderItem->item->name,
                'unit_price' => number_format($orderItem->unit_price, 2, '', ''),
                'quantity' => $orderItem->quantity,
                'tangible' => $orderItem->item->tangible
            ];
        });

        $transaction = $this->transactionService->insert($data);
        $order->transactions()->attach($transaction->id);

        return $order;
    }
}
