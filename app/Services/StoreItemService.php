<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\StoreItemModel;
use App\Repositories\StoreItemRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StoreItemService
{
    private $model;
    private $repository;

    public function __construct(StoreItemModel $model, StoreItemRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $store_item = null;
        if ($id !== null and ($store_item = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('store_item.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($store_item, $data);
    }

    public function find($id)
    {
        return $this->repository->findOrFail($id);
    }

    public function query(array $data)
    {
        return $this->repository->query($data);
    }
}
