<?php
namespace App\Services;

use App\Integrations\PagarMeIntegration;
use App\Repositories\TransactionRepository;
use ArrayObject;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TransactionService
{
    private $integration;
    private $repository;

    public function __construct(PagarMeIntegration $integration, TransactionRepository $repository)
    {
        $this->integration = $integration;
        $this->repository = $repository;
    }

    public function card_hash_key()
    {
        return $this->integration->getTransactionCardHashKey();
    }

    public function insert(array &$data)
    {
        $response = $this->integration->postTransaction($data);
        $dataArrayObject = new ArrayObject($data);
        $data['request'] = $dataArrayObject->getArrayCopy();
        $data['status'] = $response['status'];
        $data['responses'] = [$response];
        return $this->repository->insert($data);
    }

    public function postBack(array $data, $id)
    {
        if ($this->integration->validatePostBack() === false) {
            throw new NotFoundHttpException;
        }

        $transaction = $this->repository->findOrFail($id);
        $responses = $data + $transaction->responses;
        $transaction
            ->setAttribute('responses', $responses)
            ->save();
        return $transaction;
    }
}
