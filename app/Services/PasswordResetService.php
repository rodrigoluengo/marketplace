<?php
namespace App\Services;

use App\Exceptions\BusinessRuleException;
use App\Exceptions\ValidatorException;
use App\Mail\PasswordResetMail;
use App\Models\PasswordResetModel;
use App\Repositories\PasswordResetRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;

class PasswordResetService
{
    private $model;
    private $repository;
    private $userRepository;

    public function __construct(PasswordResetModel $model, PasswordResetRepository $repository, UserRepository $userRepository)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function save(array $data)
    {
        if (!$this->model->validate($data)) {
            throw new ValidatorException($this->model->getErrors());
        }
        $passwordReset = $this->repository->save($data);
        Mail::to($passwordReset->email)->send(new PasswordResetMail($passwordReset));
    }

    public function update(array $data)
    {
        if (!$this->model->updateValidate($data)) {
            throw new ValidatorException($this->model->getErrors());
        }

        $passwordReset = $this->repository->find($data['email'], $data['token']);
        if ($passwordReset == null) {
            throw new ModelNotFoundException('passwordReset.not_found');
        }

        if ($passwordReset->created_at->diffInDays(Carbon::now()) > env('PASSWORD_RESET_EXPIRE_DAYS', 2)) {
            throw new BusinessRuleException('created_at', 'created_at.expired');
        }

        $this->userRepository->updatePassword($data);
        $this->repository->delete($passwordReset);
    }
}
