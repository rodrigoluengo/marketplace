<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\FileModel;
use App\Repositories\FileRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FileService
{
    private $model;
    private $repository;

    public function __construct(FileModel $model, FileRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $file = null;
        if ($id !== null and ($file = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('file.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($file, $data);
    }

    public function find($id)
    {
        $file = $this->repository->find($id);
        if ($file === null) {
            throw new ModelNotFoundException('file.not_found');
        }
        return $file;
    }

    public function delete($id)
    {
        $file = null;
        if ($id !== null and ($file = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('file.not_found');
        }

        return $this->repository->delete($file);
    }
}
