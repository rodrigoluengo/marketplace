<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\BuyModel;
use App\Repositories\BuyRepository;
use App\Repositories\ItemRepository;

class BuyService
{
    private $model;
    private $repository;
    private $itemRepository;

    public function __construct(BuyModel $model, BuyRepository $repository, ItemRepository $itemRepository)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->itemRepository = $itemRepository;
    }

    public function push(array $data)
    {
        if (!$this->model->validate($data)) {
            throw new ValidatorException($this->model->getErrors());
        }

        return $this->repository->push($data);
    }

    public function basket(array $data)
    {
        $itemsQuantity  = $this->repository->all($data);
        $items          = $this->itemRepository->findIds(array_keys($itemsQuantity));
        $quantity       = 0;
        $total          = 0;
        $discount       = 0;
        foreach ($items as $i => &$item) {
            $item->quantity         = $itemsQuantity[$item->id];
            $item->total            = $item->quantity * $item->price;
            $item->totalDiscount    = 0;
            $quantity               += $item->quantity;
            $total                  += $item->total;
            if ($item->discount !== null) {
                $item->totalDiscount = $item->quantity * $item->discount;
                $discount += $item->totalDiscount;
            }
        }
        return [
            'items'     => $items,
            'quantity'  => $quantity,
            'total'     => $total,
            'discount'  => $discount
        ];
    }

    public function remove(array $data)
    {
        return  $this->repository->remove($data);
    }
}