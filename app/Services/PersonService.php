<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\PersonModel;
use App\Repositories\PersonRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PersonService
{
    private $model;
    private $repository;

    public function __construct(PersonModel $model, PersonRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $person = null;
        if ($id !== null and ($person = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('person.not_found');
        }

        if (!$this->model->validate($data, $id)) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id !== null) {
            return $this->repository->update($person, $data);
        }

        return $this->repository->insert($data);
    }

    public function find($id)
    {
        $person = $this->repository->find($id);

        if ($person === null) {
            throw new ModelNotFoundException('person.not_found');
        }

        return $person;
    }

    public function search(array $data)
    {
        return $this->repository->search($data);
    }
}
