<?php
namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Models\AddressModel;
use App\Repositories\AddressRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AddressService
{
    private $model;
    private $repository;

    public function __construct(AddressModel $model, AddressRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $address = null;
        if ($id !== null and ($address = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException('address.not_found');
        }

        if ($this->model->validate($data, $id) === false) {
            throw new ValidatorException($this->model->getErrors());
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($address, $data);
    }
}
