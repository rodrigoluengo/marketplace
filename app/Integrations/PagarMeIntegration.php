<?php
namespace App\Integrations;

use App\Enums\HttpStatus;
use App\Exceptions\ValidatorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use PagarMe\Sdk\PagarMe;

class PagarMeIntegration
{
    public function getTransactionCardHashKey()
    {
        $client = new Client();
        $response = $client->get(env('PAGAR_ME_ENDPOINT') . 'transactions/card_hash_key', [
            'headers' => [
                'content-type' => 'application/json'
            ],
            'json' => [
                'encryption_key' => env('PAGAR_ME_APP_ENCRYPTION_KEY')
            ]
        ]);
        if ($response->getStatusCode() === HttpStatus::OK) {
            return json_decode($response->getBody()->getContents(), true);
        }
        return null;
    }

    public function postTransaction(array $data)
    {
        $client = new Client();
        $data['api_key'] = env('PAGAR_ME_APP_KEY');
        try {
            $response = $client->post(env('PAGAR_ME_ENDPOINT') . 'transactions', [
                'headers' => [
                    'content-type' => 'application/json'
                ],
                'json' => $data
            ]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $clientException) {
            $body = json_decode($clientException->getResponse()->getBody()->getContents(), true);
            $errors = collect($body['errors'])->map(function ($error) {
                return [$error['parameter_name'] => [$error['message']]];
            })->toArray();
            throw new ValidatorException($errors);
        } catch (RequestException $e) {

        }
    }

    public function validatePostBack()
    {
        $pagarMe = new PagarMe(env('PAGAR_ME_APP_KEY'));
        $payload = file_get_contents("php://input");
        $request = app(Request::class);
        $headers = $request->headers->all();
        return $pagarMe->postback()->validateRequest(
            $payload,
            $headers['x-hub-signature']
        );
    }
}
