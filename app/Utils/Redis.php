<?php
namespace App\Utils;

use Illuminate\Support\Facades\Redis as RedisFacade;

class Redis
{
    const TWO_HOURS = 7200;

    public static function set($key, $value, int $ttl = -1)
    {
        $result = RedisFacade::set($key, $value);
        if ($ttl > 0) {
            RedisFacade::expire($key, $ttl);
        }
        return $result;
    }

    public static function setJson($key, $value, int $ttl = -1)
    {
        return self::set($key, json_encode($value), $ttl);
    }

    public static function get($key)
    {
        $keys = RedisFacade::keys($key);
        $values = [];

        foreach ($keys as $val) {
            $values[] = RedisFacade::get($val);
        }

        if (count($values) === 1) {
            return $values[0];
        }

        if (count($values) === 0) {
            return null;
        }

        return $values;
    }

    public static function getJson($key)
    {
        return json_decode(self::get($key));
    }
}