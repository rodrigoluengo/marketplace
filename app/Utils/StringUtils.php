<?php
namespace App\Utils;

class StringUtils
{
    public static function words ($value)
    {
        $value = preg_replace('/\\s+/', ' ',$value);
        $words = explode(' ', $value);
        return $words;
    }
}
