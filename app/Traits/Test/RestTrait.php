<?php
namespace App\Traits\Test;

trait RestTrait
{
    public function getJsonResponse()
    {
        return json_decode($this->response->getContent());
    }

    public function showJsonResult()
    {
        print_r($this->getJsonResponse());
    }
}
