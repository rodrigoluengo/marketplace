<?php
namespace App\Traits\Test;

use App\Enums\HttpStatus;

trait AuthTestTrait
{
    public function getToken()
    {
        $this->post('/auth', ['email' => 'email@pontualracoes.com.br', 'password' => 'secreto']);
        $this->assertResponseOk();
        if ($this->response->status() === HttpStatus::OK) {
            $response = json_decode($this->response->content());
            return $response->access_token;
        }
    }

    public function getHeaderAuthorization()
    {
        return ['Authorization' => 'Bearer ' . $this->getToken()];
    }
}
