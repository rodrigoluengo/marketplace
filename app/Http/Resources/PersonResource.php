<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'status_name' => $this->status ? 'Ativo' : 'Inativo',
            'name' => $this->name,
            'company' => $this->company,
            'gender' => $this->gender,
            'since' => $this->since,
            'document' => $this->document,
            'details' => $this->details,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];

        $address = collect($this->addresses)->filter(function ($address) {
            return $address->pivot->default;
        })->first();
        if ($address !== null) {
            $data['zipcode'] = $address->zipcode;
            $data['address'] = $address->address . ', ' . $address->number;
            $data['complement'] = $address->complement;
            $data['neighborhood'] = $address->neighborhood;
            $data['city'] = $address->city;
            $data['state'] = $address->state;
        }

        $phone = collect($this->phones)->filter(function ($phone) {
            return $phone['default'];
        })->first();

        if ($phone !== null) {
            $data['number'] = $phone['number'];
        }

        return $data;
    }
}