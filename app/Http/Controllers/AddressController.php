<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Exceptions\ValidatorException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function zipCode(Request $request)
    {
        $this->validate($request, [
            'zipcode' => 'required|regex:/^[0-9]{5}-[0-9]{3}$/'
        ]);
        $zipCode = preg_replace("/\D/", '', $request->zipcode);
        $client = new Client();
        $response = $client->request('GET', "viacep.com.br/ws/$zipCode/json/");

        if ($response->getStatusCode() !== HttpStatus::OK) {
            throw new ValidatorException(['zipcode' => ['Não foi possível fazer a busca, por favor tente novamente mais tarde']]);
        }

        $content = json_decode($response->getBody()->getContents());

        return response()->json([
            'zipcode' => $content->cep,
            'street' => $content->logradouro,
            'complement' => $content->complemento,
            'neighborhood' => $content->bairro,
            'city' => $content->localidade,
            'state' => $content->uf
        ]);
    }
}
