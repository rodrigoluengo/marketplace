<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $order = $this->service->save($request->all());
        return response()->json(['id' => $order->id], HttpStatus::CREATED);
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function delete($id)
    {
        $this->service->remove($id);
        return response()->json(null, HttpStatus::OK);
    }

    public function payment(Request $request, $id)
    {
        $this->service->payment($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }
}
