<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\StoreService;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    private $service;

    public function __construct(StoreService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $store = $this->service->save($request->all());
        return response()->json(['id' => $store->id], HttpStatus::CREATED);
    }

    public function get($id)
    {
        return $this->service->find($id);
    }
}
