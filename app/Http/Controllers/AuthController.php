<?php
namespace App\Http\Controllers;

use App\Entities\Store;
use App\Entities\User;
use App\Enums\HttpStatus;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function post(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }
        return response()->json(['error' => 'Unauthorized'], HttpStatus::UNAUTHORIZED);
    }

    public function delete()
    {
        $this->guard()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        $expires_in = $this->guard()->factory()->getTTL() * 60;
        $datetime_expires_in = Carbon::now()->addSeconds($expires_in);
        $user = Auth::user();
        $domain = null;
        $store = Store::where('user_id', $user->id)->first();
        if ($store !== null) {
            $domain = $store->domain;
        }
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
            'datetime_expires_in' => $datetime_expires_in->format('Y-m-d H:i'),
            'timezone' => $datetime_expires_in->getTimezone(),
            'store' => $domain,
            'user' => [
                'id' => $user->id,
                'email' => $user->email,
                'person' => $user->person,
                'role' => $user->role
            ]
        ]);
    }

    public function guard()
    {
        return Auth::guard();
    }

    public function get()
    {
        $user = Auth::user();
        $user = User::query()->with('person', 'person.addresses')->where('id', $user->id)->first();
        return response()->json($user, HttpStatus::OK);
    }
}