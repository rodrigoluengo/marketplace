<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\TransactionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class TransactionController extends Controller
{
    private $service;

    public function __construct(TransactionService $service)
    {
        $this->service = $service;
    }

    public function cardHashKey()
    {
        return response()->json($this->service->card_hash_key(), HttpStatus::OK);
    }

    public function post(Request $request)
    {
        return $this->service->insert($request->all());
    }

    public function postBack(Request $request, $id)
    {
        $this->service->postBack($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }
}
