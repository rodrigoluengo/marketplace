<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\FileService;
use Illuminate\Http\Request;

class FileController
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $file = $this->service->save($request->all());
        return response()->json(['id' => $file->id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function get($id)
    {
        $file = $this->service->find($id);
        return response()->download(storage_path('app/' . $file->getFolder() . '/' . $file->filename, $file->name));
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response()->json(null, HttpStatus::OK);
    }
}