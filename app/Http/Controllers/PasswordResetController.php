<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\PasswordResetService;
use DB;
use Illuminate\Http\Request;

class PasswordResetController extends Controller
{
    private $service;

    public function __construct(PasswordResetService $service)
    {
         $this->service =  $service;
    }

    public function post(Request $request)
    {
        $this->service->save($request->all());
        return response()->json(null, HttpStatus::CREATED);
    }

    public function put(Request $request)
    {
        $this->service->update($request->all());
        return response()->json(null, HttpStatus::OK);
    }
}
