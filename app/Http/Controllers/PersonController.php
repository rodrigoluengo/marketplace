<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Http\Resources\PersonResource;
use App\Services\PersonService;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    private $service;

    public function __construct(PersonService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $person = $this->service->save($request->all());
        return response()->json(['id' => $person->id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function search(Request $request)
    {
        return PersonResource::collection($this->service->search($request->all()));
    }
}
