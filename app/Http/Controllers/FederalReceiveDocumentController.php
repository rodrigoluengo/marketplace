<?php
namespace App\Http\Controllers;

use App\Services\FederalReceiveDocumentService;
use Illuminate\Http\Request;

class FederalReceiveDocumentController extends Controller
{
    private $service;

    public function __construct(FederalReceiveDocumentService $service)
    {
        $this->service = $service;
    }

    public function get(Request $request)
    {
        return $this->service->captcha($request->all());
    }

    public function post(Request $request)
    {
        return $this->service->search($request->all());
    }
}
