<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\OrderItemService;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    private $service;

    public function __construct(OrderItemService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $order_item = $this->service->save($request->all());
        return response()->json(['id' => $order_item->id, 'order_id' => $order_item->order_id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response()->json(null, HttpStatus::OK);
    }
}
