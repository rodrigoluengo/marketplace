<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $user = $this->service->save($request->all());
        return response()->json(['id' => $user->id], HttpStatus::CREATED);
    }
}
