<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Http\Resources\ItemResource;
use App\Services\ItemService;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    private $service;

    public function __construct(ItemService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $item = $this->service->save($request->all());
        return response()->json(['id' => $item->id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function query(Request $request)
    {
        return $this->service->query($request->all());
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response()->json(null, HttpStatus::OK);
    }
}
