<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $category = $this->service->save($request->all());
        return response()->json(['id' => $category->id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function query(Request $request)
    {
        return $this->service->query($request->all());
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return response()->json(null, HttpStatus::OK);
    }
}
