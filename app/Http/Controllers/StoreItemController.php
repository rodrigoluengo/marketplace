<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\StoreItemService;
use Illuminate\Http\Request;

class StoreItemController extends Controller
{
    private $service;

    public function __construct(StoreItemService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $store_item = $this->service->save($request->all());
        return response()->json(['id' => $store_item->id], HttpStatus::CREATED);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return response()->json(null, HttpStatus::OK);
    }

    public function get($id)
    {
        return response()->json($this->service->find($id), HttpStatus::OK);
    }

    public function query(Request $request)
    {
        return response()->json($this->service->query($request->all()), HttpStatus::OK);
    }
}
