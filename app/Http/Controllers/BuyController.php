<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\BuyService;
use Illuminate\Http\Request;

class BuyController extends Controller
{
    private $service;

    public function __construct (BuyService $service)
    {
        $this->service = $service;
    }

    public function push(Request $request)
    {
        $this->service->push($request->all());
        return response()->json(null, HttpStatus::OK);
    }

    public function basket(Request $request)
    {
        return response()->json($this->service->basket($request->all()), HttpStatus::OK);
    }

    public function remove($id, Request $request)
    {
        $data = $request->all();
        $data['item_id'] = $id;
        return response()->json($this->service->remove($data), HttpStatus::OK);
    }
}
