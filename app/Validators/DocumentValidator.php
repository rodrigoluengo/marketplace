<?php
namespace App\Validators;

use GSoares\Brasil\Pessoa\Cnpj;
use GSoares\Brasil\Pessoa\Cpf;
use GSoares\Brasil\Pessoa\DocumentoInvalidoException;

class DocumentValidator
{
    public function cpf_cnpj($attribute, $value, $parameters, $validator)
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        if (strlen($value) < 14) {
            return $this->cpf($attribute, $value, $parameters, $validator);
        }
        return $this->cnpj($attribute, $value, $parameters, $validator);
    }

    public function cpf($attribute, $value, $parameters, $validator)
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        if (!empty(trim($value))) {
            try {
                new Cpf($value);
                return true;
            } catch(DocumentoInvalidoException $e) {
                return false;
            }
        }
    }

    public function cnpj($attribute, $value, $parameters, $validator)
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        if (!empty(trim($value))) {
            try {
                new Cnpj($value);
                return true;
            } catch(DocumentoInvalidoException $e) {
                return false;
            }
        }
    }
}
