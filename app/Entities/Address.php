<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Address extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'zipcode',
        'street',
        'street_number',
        'complement',
        'neighborhood',
        'city',
        'state',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'metadata' => 'array'
    ];

    public $timestamps = false;
}
