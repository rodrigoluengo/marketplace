<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class PasswordReset extends Entity
{
    protected $primaryKey = 'email';

    protected $fillable = [
        'email'
    ];

    protected $dates = [
        'created_at'
    ];

    protected $casts = [
        'email' => 'string'
    ];

    public $timestamps = false;
}
