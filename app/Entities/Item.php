<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Item extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id',
        'name',
        'status',
        'details',
        'commission',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'commission' => 'double',
        'metadata' => 'array'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class);
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->belongsToMany(File::class)->orderBy('position');
    }
}
