<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Person extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'type',
        'name',
        'company',
        'gender',
        'since',
        'document',
        'details',
        'status',
        'phones',
        'metadata'
    ];

    protected $dates = [
        'since'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'phones' => 'array',
        'metadata' => 'array'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class)->withPivot(['name', 'default']);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class, 'updated_user_id');
    }
}
