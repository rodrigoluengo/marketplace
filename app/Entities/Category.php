<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Category extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id',
        'name',
        'details',
        'status',
        'commission',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'commission' => 'double',
        'metadata' => 'array',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class);
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class);
    }
}
