<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class OrderStoreItem extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'order_id',
        'item_id',
        'details',
        'quantity',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'quantity' => 'double',
        'cost' => 'double',
        'cost_total' => 'double',
        'price' => 'double',
        'value' => 'double',
        'discount_perc' => 'double',
        'discount' => 'double',
        'discount_total' => 'double',
        'commission_perc' => 'double',
        'commission' => 'double',
        'unit_price' => 'double',
        'total' => 'double',
        'lucre' => 'double',
        'metadata' => 'array'
    ];

    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function storeItem()
    {
        return $this->belongsTo(StoreItem::class);
    }
}
