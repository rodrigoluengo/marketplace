<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;
use DB;

class Order extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'person_id',
        'address_id',
        'status',
        'details',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'quantity' => 'double',
        'cost_total' => 'double',
        'value' => 'double',
        'discount_perc' => 'double',
        'discount' => 'double',
        'discount_total' => 'double',
        'commission_perc' => 'double',
        'commission' => 'double',
        'total' => 'double',
        'lucre' => 'double',
        'metadata' => 'array',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderStoreItem::class);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class, 'updated_user_id');
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class);
    }

    public function isPayed()
    {
        $payed = $this
            ->transactions()
            ->join('orders', 'orders.id', '=', 'order_transaction.order_id')
            ->where('transactions.status', 'paid')
            ->selectRaw("(SUM(transactions.amount) >= replace(orders.commission_total::text, '.', '')::integer) payed")
            ->groupBy(DB::raw("replace(orders.commission_total::text, '.', '')::integer"))
            ->groupBy('order_transaction.order_id')
            ->groupBy('order_transaction.transaction_id')
            ->first();
        return $payed === null ? false : $payed->payed;
    }
}
