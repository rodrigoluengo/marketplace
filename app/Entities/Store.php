<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Store extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'details',
        'status',
        'discount',
        'commission',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'discount' => 'double',
        'commission' => 'double',
        'metadata' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class, 'created_user_id');
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class, 'updated_user_id');
    }
}
