<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class Transaction extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'amount',
        'status',
        'payment_method',
        'request',
        'responses',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'request' => 'array',
        'responses' => 'array',
        'metadata' => 'array'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
