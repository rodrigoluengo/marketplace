<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class StoreItem extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'status',
        'stock_constraint',
        'stock',
        'cost',
        'price',
        'discount',
        'commission',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'status' => 'boolean',
        'stock_constraint' => 'boolean',
        'stock' => 'double',
        'cost' => 'double',
        'price' => 'double',
        'discount' => 'double',
        'commission' => 'double',
        'metadata' => 'array'
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class);
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->belongsToMany(File::class)->orderBy('position');
    }
}
