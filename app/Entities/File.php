<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model as Entity;

class File extends Entity
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'public',
        'filename',
        'name',
        'size',
        'mime_type',
        'position',
        'metadata'
    ];

    protected $casts = [
        'id' => 'string',
        'public' => 'boolean',
        'size' => 'integer',
        'position' => 'integer',
        'metadata' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->belongsToMany(Item::class)->withPivot('position');
    }

    public function getFolder($public = null)
    {
        return ($public === null ? $this->public : $public) ? 'public' : 'files';
    }

    public function createdUser()
    {
        return $this->belongsTo(User::class);
    }

    public function updatedUser()
    {
        return $this->belongsTo(User::class);
    }
}
