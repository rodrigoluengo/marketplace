<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mockery\Generator\StringManipulation\Pass\Pass;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $passwordReset;

    public function __construct(\App\Entities\PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    public function build()
    {
        return $this->view('emails.password-reset');
    }
}
