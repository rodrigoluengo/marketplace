<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 01/03/18
 * Time: 16:18
 */

namespace App\Jobs;

use \Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;


class ProcessPodcast implements ShouldQueue
{
    public $tries = 5;


    public function retryUntil()
    {
        return Carbon::now()->addSeconds(5);
    }
}