<?php

namespace App\Jobs;

use Carbon\Carbon;

class ExampleJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        file_put_contents(storage_path('app/public/teste_' . Carbon::now()->format('YmdHs') . '.txt'), Carbon::now());
    }
}
