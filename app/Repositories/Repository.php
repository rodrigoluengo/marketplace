<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;

class Repository implements RepositoryInterface
{
    protected $entityClass;

    public function find($id)
    {
        return app($this->entityClass)->find($id);
    }

    public function findOrFail($id)
    {
        return app($this->entityClass)->findOrFail($id);
    }

    public function findAll()
    {
        return app($this->entityClass)->all();
    }

    public function insert(array $data)
    {
        return app($this->entityClass)->create($data);
    }

    public function update($entity, array $data)
    {
        return $entity->fill($data)->update();
    }

    public function delete($entity)
    {
        return $entity->delete();
    }
}
