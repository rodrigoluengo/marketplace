<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Item;
use App\Entities\Owner;
use App\Entities\Store;
use App\Entities\StoreItem;
use App\Utils\StringUtils;
use Auth;

class ItemRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Item::class;

    public function insert(array $data)
    {
        $item = new Item($data);
        $item->createdUser()->associate(Auth::user());
        $item->save();
        return $item;
    }

    public function update($item, array $data)
    {
        $item->fill($data);
        $item->updatedUser()->associate(Auth::user());
        $item->update();
        return $item;
    }

    public function find($id)
    {
        return Item::query()
            ->with(['category', 'images'])
            ->where('id', $id)
            ->first();
    }

    public function findIds(array $ids)
    {
        $owner = app(Owner::class);
        return Item::query()
            ->with(['category', 'images'])
            ->where('owner_id', $owner->id)
            ->whereIn('id', $ids)
            ->get();
    }

    public function query(array $data)
    {
        $query = Item::query()
                    ->with('images')
                    ->join('categories', 'items.category_id', '=', 'categories.id');

        $status = array_get($data, 'status');
        if ($status !== null) {
            $query->where('items.status', $status);
        }

        $category_id = array_get($data, 'category_id');
        if ($category_id !== null) {
            $query->where('items.category_id', $category_id);
        }

        $search = array_get($data, 'search');
        if ($search !== null) {
            $query->where(function ($query) use($search) {
                $words = StringUtils::words($search);
                foreach ($words as $word) {
                    $query
                        ->where('items.name', 'ILIKE', "%$word%")
                        ->orWhere('items.details', 'ILIKE', "%$word%")
                        ->orWhere('categories.name', 'ILIKE', "%$word%")
                        ->orWhere('items.id::text', $word);
                }
            });
        }

        $id = array_get($data, 'id');
        if ($id !== null) {
            if (is_array($id)) {
                $query->whereIn('items.id', $id);
            }
        }

        $orderBy = array_get($data, 'orderBy', ['items.name' => 'asc']);
        if (is_array($orderBy)) {
            foreach ($orderBy as $column => $direction) {
                $query->orderBy($column, $direction);
            }
        }

        $query->select([
            'items.id',
            'items.category_id',
            'categories.name as category_name',
            'items.name',
            'items.details',
            'items.status',
            'items.commission'
        ])->selectRaw("case when items.status = true then 'Ativo' else 'Inativo' end status_name");

        return $query->paginate();
    }

    public function stock(array $data)
    {
        $item_id = $data['item_id'];
        $store = app(Store::class);
        if ($store->owner) {
            $owner = $store->owner;
        } else {
            $owner = app(Owner::class);
        }

        return Item::query()
            ->where('id', $item_id)
            ->where('owner_id', $owner->id)
            ->selectRaw('coalesce(items.stock, 0) as stock')
            ->first()
            ->stock;
    }

    public function hasStoreItems($id)
    {
        return StoreItem::where('item_id', $id)->exists();
    }
}