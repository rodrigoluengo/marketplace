<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Category;
use App\Entities\Item;
use App\Entities\Owner;
use App\Entities\Store;
use Auth;
use DB;
use Illuminate\Support\Str;

class CategoryRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Category::class;

    public function insert(array $data)
    {
        $category = new Category($data);
        $category->createdUser()->associate(Auth::user());
        $category->save();
        return $category;
    }

    public function update($category, array $data)
    {
        $category->fill($data);
        $category->updatedUser()->associate(Auth::user());
        $category->update();
        return $category;
    }

    public function query(array $data)
    {
        $query = Category::query()->orderBy('name');

        $category_id = array_get($data, 'category_id');
        if ($category_id !== null) {
            $query->where('category_id', $category_id);
        }

        $status = array_get($data, 'status');
        if ($status !== null) {
            $query->where('status', $status);
        }

        $search = array_get($data, 'search');
        if ($search !== null) {
            $query->where(function ($query) use($search) {
                $words = explode(' ', $search);
                foreach ($words as $word) {
                    $query
                        ->where('name', 'ilike', "%$word%")
                        ->orWhere('details', 'ilike', "%$word%");
                }
            });
        }

        $orderBy = array_get($data, 'orderBy', ['name' => 'asc']);
        foreach ($orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query->get();
    }

    public function categoryExists($category_id): bool
    {
        return Category::query()->where('category_id', $category_id)->exists();
    }

    public function itemExists($category_id): bool
    {
        return Item::query()->where('category_id', $category_id)->exists();
    }
}
