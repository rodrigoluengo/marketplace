<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Order;
use App\Entities\OrderStoreItem;
use App\Entities\Store;
use Auth;
use DB;
use Illuminate\Database\QueryException;

class OrderRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Order::class;

    private $personRepository;
    private $orderItemRepository;

    public function __construct(PersonRepository $personRepository, OrderItemRepository $orderItemRepository)
    {
        $this->personRepository = $personRepository;
        $this->orderItemRepository = $orderItemRepository;
    }

    public function insert(array $data)
    {
        $store = app(Store::class);
        $order = new Order($data);
        $order->store()->associate($store);
        if (Auth::check()) {
            $order->createdUser()->associate(Auth::user());
        }
        DB::beginTransaction();
        try {
            $order->save();
            $order_items = array_get($data, 'order_items');
            if ($order_items !== null) {
                foreach ($order_items as &$order_item) {
                    $order_item['order_id'] = $order->id;
                    $this->orderItemRepository->insert($order_item);
                }
            }
            DB::commit();
            return $order;
        } catch (QueryException $exception) {
            DB::rollback();
            return null;
        }
    }

    public function existsStore($id, $store_id)
    {
        return Order::query()->where('id', $id)->where('store_id', $store_id)->exists();
    }

    public function update($order, array $data)
    {
        $order->fill($data);
        if (Auth::check()) {
            $order->updatedUser()->associate(Auth::user());
        }
        DB::beginTransaction();
        try {
            $order->update();
            $order_items = array_get($data, 'order_items');
            $order_items_id = [];
            if ($order_items !== null) {
                foreach ($order_items as &$order_item_data) {
                    $order_item = $this->orderItemRepository->findByItemIdAndOrderId($order_item_data['item_id'], $order->id);
                    if ($order_item === null) {
                        $order_item_data['order_id'] = $order->id;
                        $order_item = $this->orderItemRepository->insert($order_item_data);
                    } else {
                        $this->orderItemRepository->update($order_item, $order_item_data);
                    }
                    $order_items_id[] = $order_item->id;
                }
            }
            OrderStoreItem::query()->where('order_id', $order->id)->whereNotIn('id', $order_items_id)->delete();
            DB::commit();
        } catch (QueryException $exception) {
            DB::rollback();
            return null;
        }
        return $order;
    }

    public function find($id)
    {
        $store = app(Store::class);
        return Order::query()
                        ->with(
                                'person',
                                'address',
                                'orderItems',
                                'orderItems.item',
                                'createdUser',
                                'updatedUser'
                        )
                        ->where('store_id', $store->id)
                        ->where('id', $id)->first();
    }
}
