<?php
namespace App\Repositories;

use App\Exceptions\ValidatorException;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SessionCookieJar;
use Symfony\Component\DomCrawler\Crawler;
use Carbon\Carbon;

class FederalReceiveDocumentRepository
{
    const URL_CPF_REQUEST = 'https://www.receita.fazenda.gov.br/Aplicacoes/SSL/ATCTA/CPF/ConsultaSituacao/ConsultaPublicaSonoro.asp';
    const URL_CPF_RESPONSE = 'https://www.receita.fazenda.gov.br/Aplicacoes/SSL/ATCTA/CPF/ConsultaSituacao/ConsultaPublicaExibir.asp';
    const URL_CPF_CAPTCHA = 'https://www.receita.fazenda.gov.br/Aplicacoes/SSL/ATCTA/CPF/captcha/gerarCaptcha.asp';

    const URL_CNPJ_REQUEST = 'http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/Cnpjreva_Solicitacao2.asp';
    const URL_CNPJ_CAPTCHA = 'http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/captcha/gerarCaptcha.asp';

    public function captchaCpf()
    {
        $cookies = new SessionCookieJar('CPF_SESSION_STORAGE');
        $client = new Client([
            'cookies' => $cookies,
            'curl' => [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSLVERSION => 4
            ]
        ]);

        $response   = $client->request('GET', self::URL_CPF_REQUEST);
        $headers    = $response->getHeaders();
        $cookie     = $headers['Set-Cookie'][0];

        $ch = curl_init(self::URL_CPF_CAPTCHA);
        $options = array(
            CURLOPT_COOKIEJAR => 'cookiejar',
            CURLOPT_HTTPHEADER => array(
                'Pragma: no-cache',
                'Origin: https://www.receita.fazenda.gov.br',
                'Host: www.receita.fazenda.gov.br',
                'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:32.0) Gecko/20100101 Firefox/32.0',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
                'Accept-Encoding: gzip, deflate',
                'Referer: ' . self::URL_CPF_REQUEST,
                "Cookie: $cookie",
                'Connection: keep-alive'
            ),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_BINARYTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSLVERSION => 4
        );
        curl_setopt_array($ch, $options);
        $img = curl_exec($ch);
        curl_close($ch);

        return ['cookie' => $cookie, 'image' => 'data:image/png;base64,' . base64_encode($img)];
    }

    public function captchaCnpj()
    {
        $cookies = new SessionCookieJar('CNPJ_SESSION_COOKIE_JAR');
        $client = new Client([
            'cookies' => $cookies
        ]);

        $response   = $client->request('GET', self::URL_CNPJ_REQUEST);
        $headers    = $response->getHeaders();
        $cookie     = $headers['Set-Cookie'][0];

        $ch = curl_init(self::URL_CNPJ_CAPTCHA);
        $options = [
            CURLOPT_COOKIEJAR => 'cookiejar',
            CURLOPT_HTTPHEADER => [
                'Pragma: no-cache',
                'Origin: http://www.receita.fazenda.gov.br',
                'Host: www.receita.fazenda.gov.br',
                'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:32.0) Gecko/20100101 Firefox/32.0',
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
                'Accept-Encoding: gzip, deflate',
                'Referer: ' . self::URL_CNPJ_REQUEST,
                "Cookie: flag=1; $cookie",
                'Connection: keep-alive'
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_BINARYTRANSFER => true
        ];
        curl_setopt_array($ch, $options);
        $img = curl_exec($ch);
        curl_close($ch);

        return ['cookie' => $cookie, 'captcha' => 'data:image/png;base64,' . base64_encode($img)];
    }

    public function searchCpf($data)
    {
        $document       = array_get($data, 'document');
        $since          = array_get($data, 'since');
        $since          = Carbon::createFromFormat('Y-m-d', $since)->format('d/m/Y');
        $cookie         = array_get($data, 'cookie', '');
        $captcha_text   = array_get($data, 'captcha_text');

        $cookie = explode(';', $cookie);
        $cookie = $cookie[0];

        $cookieJar = new SessionCookieJar('CPF_SESSION_STORAGE');
        $client = new Client([
            'cookies' => $cookieJar,
            'curl' => [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSLVERSION => 4
            ]
        ]);

        $param = array(
            'txtCPF' => preg_replace('/[^0-9]/', '', $document),
            'txtNascimento' => $since,
            'txtTexto_captcha_serpro_gov_br' => $captcha_text,
            'Enviar' => 'Consultar'
        );

        $response = $client->request('POST', self::URL_CPF_RESPONSE, [
            'form_params' => $param,
            'headers' => [
                'Origin' => 'http://www.receita.fazenda.gov.br',
                'Host' => 'www.receita.fazenda.gov.br',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:32.0) Gecko/20100101 Firefox/32.0',
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9, */* ;q=0.8',
                'Accept-Language' => 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
                'Accept-Encoding' => 'gzip, deflate',
                'Referer' => self::URL_CPF_REQUEST,
                'Connection' => 'keep-alive',
                'Cookie' => $cookie
            ]
        ]);

        $body = $response->getBody();

        $html = [];
        while (!$body->eof()) {
            $html[] = $body->read(1024);
        }
        $crawler = new Crawler(implode($html));

        $mensagemErro = $crawler->filter('.mensagemErro');
        if (count($mensagemErro) > 0) {
            throw new ValidatorException(['capcha' => [$mensagemErro->html()]]);
        }

        $clConteudoDados = $crawler->filter('span.clConteudoDados');
        $clConteudoComp = $crawler->filter('span.clConteudoComp');
        $retorno = [
            'document' => preg_replace('/[^0-9]/', '', $document),
            'name' => trim(str_replace('Nome da Pessoa Física: ', '', $clConteudoDados->eq(1)->filter('b')->html())),
            'since' => trim(str_replace('Data de Nascimento: ', '', $clConteudoDados->eq(2)->filter('b')->html())),
            'status' => str_replace('Situação Cadastral: ', '', $clConteudoDados->eq(3)->filter('b')->html()),
            'status_date' => str_replace('Data da Inscrição: ', '', $clConteudoDados->eq(4)->filter('b')->html()),
            'digito_verificador' => str_replace('Digito Verificador: ', '', $clConteudoDados->eq(5)->filter('b')->html()),
            'hora_emissao' => str_replace('Hora de emissão: ', '', $clConteudoComp->eq(0)->filter('b')->first()->html()),
            'data_emissao' => str_replace('Data de emissão: ', '', $clConteudoComp->eq(0)->filter('b')->last()->html()),
            'codigo_controle' => str_replace('Código de controle: ', '', $clConteudoComp->eq(1)->filter('b')->html())
        ];

        return $retorno;
    }

}
