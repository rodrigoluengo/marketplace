<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\File;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mimey\MimeTypes;


class FileRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = File::class;

    public function find($id)
    {
        return File::query()
            ->where('user_id', Auth::user()->user_id)
            ->where('id', $id)
            ->first();
    }

    public function insert(array $data)
    {
        $target = array_get($data, 'target');
        $id = array_get($data, 'id');
        $public = array_get($data, 'public', false);
        $position = array_get($data, 'position');

        $uploadedFile = $data['file'];
        $uuid = DB::selectOne('select uuid_generate_v4() as uuid')->uuid;
        $mimeTypes = new MimeTypes;
        $filename = $uuid . '.' . $mimeTypes->getExtension($uploadedFile->getMimeType());
        $file = new File([
            'public' => $public,
            'filename' => $filename,
            'name' => $uploadedFile->getClientOriginalName(),
            'size' => $uploadedFile->getSize(),
            'mime_type' => $uploadedFile->getMimeType(),
            'position' => $position
        ]);
        $file->setAttribute('user_id',         Auth::user()->user_id);
        $file->setAttribute('created_user_id',  Auth::user()->id);
        $file->save();

        $uploadedFile->storeAs($file->getFolder(), $filename);
        if ($target !== null && $id !== null) {
            $file->$target()->attach($id);
        }
        return $file;
    }

    public function update($file, array $data)
    {
        $public         = (Boolean) array_get($data, 'public', false);
        $position       = array_get($data, 'position');
        $uploadedFile   = array_get($data, 'file');

        if ($uploadedFile === null) {
            if ($file->public !== $public) {
                Storage::move($file->getFolder() . '/' . $file->filename, $file->getFolder($public) . '/' . $file->filename);
            }
        } else {
            Storage::delete($file->getFolder() . '/' . $file->filename);
            $uuid       = DB::selectOne('select uuid_generate_v4() as uuid')->uuid;
            $mimeTypes  = new MimeTypes;
            $filename   = $uuid . '.' . $mimeTypes->getExtension($uploadedFile->getMimeType());
            $file->fill([
                'filename'  => $filename,
                'name'      => $filename,
                'size'      => $uploadedFile->getSize(),
                'mime_type' => $uploadedFile->getMimeType()
            ]);
            $uploadedFile->storeAs($file->getFolder($public), $filename);
        }
        $file->setAttribute('public',           $public);
        $file->setAttribute('position',         $position);
        $file->setAttribute('updated_user_id',  Auth::user()->id);
        $file->update();

        return $file;
    }

    public function delete($file)
    {
        Storage::delete(($file->public ? 'public' : 'file') . '/' . $file->filename);
        return $file->delete();
    }
}