<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Store;
use App\Entities\Transaction;

class TransactionRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Transaction::class;

    public function insert(array $data)
    {
        $store = app(Store::class);
        $transaction = new Transaction($data);
        $transaction->store()->associate($store);
        $transaction->save();
        return $transaction;
    }
}
