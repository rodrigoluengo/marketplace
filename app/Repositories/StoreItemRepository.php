<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Store;
use App\Entities\StoreItem;
use Illuminate\Support\Facades\Auth;

class StoreItemRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = StoreItem::class;

    public function insert(array $data)
    {
        $store = app(Store::class);
        $store_item = new StoreItem($data);
        $store_item->store()->associate($store);
        $store_item->setAttribute('item_id', $data['item_id']);
        $store_item->createdUser()->associate(Auth::user());
        $store_item->save();
        return $store_item;
    }

    public function update($store_item, array $data)
    {
        $store_item->updatedUser()->associate(Auth::user());
        $store_item->save();
        return $store_item;
    }

    public function query(array $data)
    {
        $query = StoreItem::query()
                            ->with(['item', 'item.category', 'item.images'])
                            ->join('items', 'store_items.item_id', '=', 'items.id')
                            ->join('categories', 'items.category_id', '=', 'categories.id');


    }

}
