<?php
namespace App\Repositories;

use App\Entities\Owner;
use App\Utils\Redis;
use Illuminate\Support\Facades\Redis as RedisFacade;

class BuyRepository
{
    public function push(array $data)
    {
        $owner = app(Owner::class);
        $quantity = $data['quantity'];
        return Redis::set("buy:{$owner->id}:{$data['session_id']}:{$data['item_id']}", $quantity);
    }

    public function quantity($item_id, $session_id = null)
    {
        $owner = app(Owner::class);
        $quantity = Redis::get("buy:{$owner->id}:" . ($session_id == null ? '*' : $session_id) . ":$item_id");
        if ($quantity !== null) {
            if (is_array($quantity) === false) {
                return $quantity;
            }
            return array_reduce($quantity, function ($total, $quantity) { return $total += $quantity; });
        }
        return 0;
    }

    public function all(array $data)
    {
        $session_id = array_get($data, 'session_id');
        $owner      = app(Owner::class);
        $keys       = RedisFacade::keys("buy:{$owner->id}:" . ($session_id == null ? '*' : $session_id) . ":*");
        $items      = [];
        foreach ($keys as $key) {
            preg_match("/^buy+\:+[1-9]{1,}+\:+[a-zA-Z0-9\*_]{1,}+\:+([0-9]{1,})$/", $key, $matches);
            $item_id = $matches[1];
            $quantity = RedisFacade::get($key);
            $items[$item_id] = $quantity;
        }
        return $items;
    }

    public function remove(array $data)
    {
        $owner = app(Owner::class);
        return RedisFacade::del("buy:{$owner->id}:{$data['session_id']}:{$data['item_id']}");
    }
}
