<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\OrderStoreItem;
use App\Entities\Owner;
use App\Entities\Store;

class OrderItemRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = OrderStoreItem::class;

    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function findByItemIdAndOrderId($item_id, $order_id)
    {
        $store = app(Store::class);
        $order_item = OrderStoreItem::query()
                                    ->join('orders', 'order_items.order_id', '=', 'orders.id')
                                    ->where('item_id', $item_id)
                                    ->where('order_id', $order_id)
                                    ->where('orders.store_id', $store->id)
                                    ->select(['order_items.id', 'order_items.order_id', 'order_items.quantity'])
                                    ->first();
        return $order_item;
    }

    public function quantity($item_id, $order_id = null)
    {
        $query = OrderStoreItem::query()
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->where('order_items.item_id', $item_id)
            ->whereNull('status');

        if ($order_id !== null) {
            $query->where('order_items.order_id', '<>', $order_id);
        }

        return $query->selectRaw('sum(order_items.quantity) as quantity')->first()->quantity;
    }

    public function insert(array $data)
    {
        $order_id   = array_get($data, 'order_id');
        $item_id    = array_get($data, 'item_id');
        $quantity   = array_get($data, 'quantity');
        $order_item = new OrderStoreItem([
            'order_id' => $order_id,
            'item_id' => $item_id,
            'quantity' => $quantity
        ]);
        $order_item->save();
        return $order_item;
    }

    public function update($order_item, array $data)
    {
        $quantity = array_get($data, 'quantity');
        $order_item->fill(['quantity' => $quantity]);
        $order_item->update();
        return $order_item;
    }
}
