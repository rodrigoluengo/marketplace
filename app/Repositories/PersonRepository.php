<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Address;
use App\Entities\Person;
use Auth;
use DB;
use Illuminate\Database\QueryException;

class PersonRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Person::class;

    public function insert(array $data)
    {
        DB::beginTransaction();
        try {
            $person = new Person($data);
            $person->owner()->associate(Auth::user()->owner);
            $person->createdUser()->associate(Auth::user());
            $person->save();

            $addresses = $data['addresses'];
            foreach ($addresses as $value) {
                $address = new Address($value);
                $person->addresses()->save($address, [
                    'name' => $value['name'],
                    'default' => $value['default']
                ]);
            }

            DB::commit();
            return $person;
        } catch (QueryException $exception) {
            DB::rollback();
            return null;
        }
    }

    public function update($person, array $data)
    {
        DB::beginTransaction();
        try {
            $person->fill($data);
            $person->updatedUser()->associate(Auth::user());
            $person->update();

            $addresses = $data['addresses'];
            $addresses_id = [];
            foreach ($addresses as $value) {
                $address_id = array_get($value, 'id');
                if ($address_id === null) {
                    $address = new Address($value);
                    $person->addresses()->save($address, [
                        'name' => $value['name'],
                        'default' => $value['default']
                    ]);
                } else {
                    $address = $person->addresses()->findOrFail($address_id);
                    $address->fill($value);
                    $person->addresses()->updateExistingPivot($address_id, [
                        'name' => $value['name'],
                        'default' => $value['default']
                    ]);
                    $address->save();
                }

                $addresses_id[] = $address->id;
            }
            $person->addresses()->whereNotIn('addresses.id', $addresses_id)->delete();

            DB::commit();
            return $person;
        } catch (QueryException $exception) {
            DB::rollback();
            return null;
        }
    }

    public function find($id)
    {
        return Person::query()
            ->with('addresses')
            ->where('id', $id)
            ->first();
    }

    public function search(array $data)
    {
        $search = array_get($data, 'search');
        $orderBy = array_get($data, 'orderBy', ['name' => 'asc']);

        $query = Person::query()
                    ->with('addresses')
                    ->where('owner_id', Auth::user()->owner_id);

        if ($search !== null) {
            $query->where(function ($query) use($search) {

                $query
                    ->where('people.name', 'ILIKE', "%$search%")
                    ->orWhere('people.company', 'ILIKE', "%$search%");

                if (is_numeric($search)) {
                    $query->orWhere('people.id', $search);
                }

            });
        }

        $status = array_get($data, 'status');
        if ($status !== null) {
            $query->where('people.status', $status);
        }

        if (is_array($orderBy)) {
            foreach ($orderBy as $column => $direction) {
                $query->orderBy($column, $direction);
            }
        }

        return $query->paginate();
    }

    public function addressExists($person_id, $address_id): bool
    {
        return DB::selectOne("SELECT EXISTS(SELECT 1 FROM address_person WHERE person_id = ? AND address_id = ?) AS address_exists", [
            $person_id,
            $address_id
        ])->address_exists;
    }
}