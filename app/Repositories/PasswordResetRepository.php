<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\PasswordReset;
use Carbon\Carbon;
use DB;

class PasswordResetRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = PasswordReset::class;

    public function save(array $data)
    {
        $email = $data['email'];
        $token = str_random(64);
        $query = "INSERT INTO password_resets(email, token, created_at) 
                  VALUES (?, ?, ?) 
                  ON CONFLICT(email) DO UPDATE SET token = EXCLUDED.token, created_at = EXCLUDED.created_at";
        DB::statement($query, [$email, $token, Carbon::now()]);
        return $this->find($email);
    }

    public function find($email, $token = null)
    {
        $query = PasswordReset::query()->where('email', $email);
        if ($token !== null) {
            $query->where('token', $token);
        }
        return $query->first();
    }
}
