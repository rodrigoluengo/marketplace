<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Store;
use Auth;

class StoreRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Store::class;

    public function insert(array $data)
    {
        $store = new Store($data);
        $store->owner()->associate(Auth::user()->owner);
        $store->createdUser()->associate(Auth::user());
        $store->save();
        return $store;
    }

    public function findIdOrDomain($id)
    {
        $query = Store::query();

        if (is_numeric($id)) {
            return $query->find($id);
        }

        return $query->where('domain', $id)->first();
    }
}
