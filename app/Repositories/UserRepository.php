<?php
namespace App\Repositories;

use App\Contracts\Repository\RepositoryInterface;
use App\Entities\Owner;
use App\Entities\Person;
use App\Entities\User;
use DB;
use Illuminate\Database\QueryException;

class UserRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = User::class;

    public function updatePassword(array $data)
    {
        return User::query()->where('email', $data['email'])->update([
            'password' => app('hash')->make($data['password'])
        ]);
    }

    public function insert(array $data)
    {
        DB::beginTransaction();
        try {
            $owner = app(Owner::class);
            $user = new User($data);
            $user->owner()->associate($owner);
            $user->save();
            if (array_key_exists('name', $data)) {
                $person = new Person(['name' => $data['name']]);
                $person->owner()->associate($owner);
                $person->user()->associate($user);
                $person->save();
            }
            DB::commit();
            return $user;
        } catch(QueryException $exception) {
            DB::rollback();
            return null;
        }
    }
}
