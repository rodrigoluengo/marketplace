<?php
namespace App\Providers;

use App\Entities\OrderStoreItem;
use App\Observers\OrderItemObserver;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('cpf', 'App\Validators\DocumentValidator@cpf');
        Validator::extend('cnpj', 'App\Validators\DocumentValidator@cnpj');
        Validator::extend('cpf_cnpj', 'App\Validators\DocumentValidator@cpf_cnpj');

        //OrderStoreItem::observe(OrderItemObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
