<?php
namespace App\Providers;

use App\Entities\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UserServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->singleton(User::class, function ($app) {
            if (Auth::check()) {
                return Auth::user()->user;
            }
            throw new UnauthorizedHttpException("Unauthorized");
        });
    }

}
