<?php
namespace App\Providers;

use App\Entities\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StoreServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->singleton(Store::class, function ($app) {
            $request = app(Request::class);
            $host = preg_replace('/^[www\.]{0,}/', '', $request->header('host'));
            $store = Store::where('host', $host)->first();
            if ($store !== null) {
                return $store;
            }
            if (Auth::check()) {
                $store = Auth::user()->store;
                if ($store !== null) {
                    return $store;
                }
            }
            throw new NotFoundHttpException('Not found');
        });
    }
}
