<?php
namespace App\Contracts\Repository;

interface RepositoryInterface
{
    public function find($id);

    public function findOrFail($id);

    public function findAll();

    public function insert(array $data);

    public function update($entity, array $data);

    public function delete($entity);
}
