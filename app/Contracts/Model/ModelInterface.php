<?php
namespace App\Contracts\Model;

interface ModelInterface
{
    public function validate(array &$data, $id = null): bool;
}
